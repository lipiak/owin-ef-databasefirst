﻿namespace OwinWebApi.Identity
{
    internal class IdentityUser
    {
        public object UserName { get; internal set; }
    }
}