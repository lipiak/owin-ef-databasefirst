﻿using Microsoft.AspNet.Identity;
using OwinWebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwinWebApi.Identity
{
    class AuthRepository : IDisposable
    {
        private OwinEntities _owinDataContext;

        public AuthRepository()
        {
            _owinDataContext = new OwinEntities();
        }


        public IdentityResult RegisterUser(UserRegisterViewModel userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.FullName
            };

            var result = _owinDataContext.Users.Add(new Users
            {
                FullName = userModel.FullName,
                Email = userModel.Email,
                Password = userModel.Password
            });

            if (result != null)
            {
                _owinDataContext.SaveChanges();
                return IdentityResult.Success;
            }

            else
                return new IdentityResult("Nie udało się utworzyć encji");
        }

        public IdentityUser FindUser(string email, string password)
        {
            var user = _owinDataContext.Users.Where(x => x.Email == email && x.Password == password).FirstOrDefault();
            
            if(user != null)
            {
                return new IdentityUser()
                {
                    UserName = user.FullName
                };
            }

            return null;
        }

        public void Dispose()
        {
            _owinDataContext.Dispose();
        }
    }
}