﻿using OwinWebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace OwinWebApi.Controllers
{
    [Authorize]
    public class ContactsController : ApiController
    {
        public IHttpActionResult Get()
        {
            var serializer = new JavaScriptSerializer();

            var contacts = new ContactViewModel[]
            {
                new ContactViewModel() { FullName = "Artur Borubar", Phone = "123456" },
                new ContactViewModel() { FullName = "Jebiesuki Nabosaka",  Phone = "65435" }
            };

            return Ok(serializer.Serialize(contacts));
        }
    }
}
