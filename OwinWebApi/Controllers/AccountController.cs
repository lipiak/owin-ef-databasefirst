﻿using Microsoft.AspNet.Identity;
using OwinWebApi.Identity;
using OwinWebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OwinWebApi.Controllers
{
    public class AccountController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Register(UserRegisterViewModel vm)
        {
            using (var authRepo = new AuthRepository())
            {
                IdentityResult result = authRepo.RegisterUser(vm);

                if (result.Succeeded)
                {
                    return Ok();
                }
                else return BadRequest();
            }
        }
    }
}
